from distutils.core import setup

setup(
   name='PySatLogs',
   version='1',
   packages=['Code'],
   package_data={'Code': ['Test_Input_Files/*.txt']},
   include_package_data=True,
   license='MIT',
   long_description=open('Readme.txt').read(),
)

