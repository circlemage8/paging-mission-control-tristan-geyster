#import necessary modules for time proessing to ensure valid comparisons are made with day/month/year/leapyear transitions
import time
import datetime as dt

#Create a class for storing satellite logs that can be expanded or maintained as changes in source structure change
class SatLog:
    def __init__(self,logtime,satid,RHLim,YHLim,YLLim,RLLim,Raw,Comp):
        self.satid=satid
        self.logtime=logtime
        self.RHLim=RHLim
        self.YHLim=YHLim
        self.YLLim=YLLim
        self.RLLim=RLLim
        self.Raw=Raw
        self.Comp=Comp

    #create a method for converting the given text formatted date into a float of the minutes for time comparisons
    def logtimemin(self):
        processedtime=0.0
        temptime=dt.datetime(int(self.logtime[0:4]),int(self.logtime[4:6]),int(self.logtime[6:8]),int(self.logtime[9:11]),int(self.logtime[12:14]),int(self.logtime[15:17]),int(self.logtime[18:21])).timetuple()
        processedtime=time.mktime(temptime)/60
        return(processedtime)