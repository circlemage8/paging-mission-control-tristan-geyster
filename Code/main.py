import SatLog as SL #includes a class for satellite logs and a function to convert time to minutes

# Ask user to enter logfile with path for procesing
logfile=input("Enter LogFile with path:")

with open(logfile) as inputlog:
    #precreate list variables for processing the text data into the satlog object class and preset variables for proper json formatting of output
    rawlist1 = inputlog.readlines()
    rawlist2=[]
    loglist=[]
    satlist=[]
    formattingflag=0
    formattingcounter = 0

    #iterate through rawdata to clean out linebreaks and split into lists by separator. No error checking for ingest of source data per guidance.
    for i in range(len(rawlist1)):
        rawlist2.append((rawlist1[i].replace('\n','')).split('|'))

    #iterate through data to change datatypes and put into satlog class objects inside list
    for j in range(len(rawlist2)):
        loglist.append(SL.SatLog(rawlist2[j][0], int(rawlist2[j][1]) , int(rawlist2[j][2]) , int(rawlist2[j][3]),int(rawlist2[j][4]),int(rawlist2[j][5]),float(rawlist2[j][6]),rawlist2[j][7]))
        satlist.append(int(rawlist2[j][1]))

    #create unique set of total sat ids for iteration
    satunique=set(satlist)

    #Create first line of output
    print('[')

    #nested iteration to go through all satellite ids and then go through all component types in case a single satellite has multiple
    for k in satunique:
        for m in {'TSTAT','BATT'}:
            #precreate and reset to blank a list of just the logs for the current iteration of satellite and component
            processinglist = []

            #create and reset to blank a counter list tracking flagged entries and their timestamps
            counter = []

            #Add only iteration specific log entries to the processinglist variable
            for l in range(len(loglist)):
                if k==loglist[l].satid and m==loglist[l].Comp:
                    processinglist.append(loglist[l])

            #Iterate through all entries for a satellite/component combination to see if entries are flagged
            for n in range(len(processinglist)):
                #preset and reset severity variable for each iteration
                severity=''

                #Check if the value is a warning value and if so add its time in minutes and time for formatting to the counter list
                if (m=='BATT' and processinglist[n].Raw < processinglist[n].RLLim) or (m=='TSTAT' and processinglist[n].Raw > processinglist[n].RHLim):
                    counter.append([processinglist[n].logtimemin(),processinglist[n].logtime])

                #Iterate through the counter list and delete entries more than 5 minutes older than the current log entry being processed
                for o in range(len(counter)):
                    if processinglist[n].logtimemin() - counter[0][0] > 5.0:
                        del(counter[0])

                #set the severity text based on the current iteration component
                if m=='BATT':
                    severity='RED LOW'
                elif m=='TSTAT':
                    severity='RED HIGH'

                #check if the counter list has 3 or more entries.
                if len(counter) >= 3:
                    #Change time formatting from given time format to desired output format
                    formattedtime = (counter[0][1]).replace(' ', 'T') + 'Z'
                    formattedtime = formattedtime[:4] + '-' + formattedtime[4:]
                    formattedtime = formattedtime[:7] + '-' + formattedtime[7:]

                    #For any entry after the first handle JSON comma formatting
                    if formattingcounter>0:
                        print('    },')

                    #output the opening JSON formatted entry for a satellite with problems using the timestamp of the first of the problematic entries in the 5 minute range
                    print('    {')
                    print(f'        "satelliteId": {processinglist[n].satid}')
                    print(f'        "severity": {severity}')
                    print(f'        "component": {processinglist[n].Comp}')
                    print(f'        "timestamp": {formattedtime}')

                    #increase the counters and flag counters which are used for proper json formatting
                    formattingcounter=formattingcounter+1
                    formattingflag=formattingflag+1

    #add a closing json bracket for the final entry but only if there is at least one entry
    if formattingflag>0:
        print('    }')

    #General closing bracket for results
    print(']')