Example coding project for Paging Mission Control Challenge
By Tristan Geyster

This is a project which takes in known log file structures for satellite sensor data and outputs JSON entries if there
are three or more red flagged entries in a 5 minute period. This has handling for both battery and temperature sensor
logs.

Dependencies:
This has dependencies on the following python standard modules in Python 3. No manually installed modules are needed.

time
datetime

Installation instructions
1. Install Python3.x if not already present.
2. Install Pip within Python3 if not already present
3. Create a desired location folder for the application
4. Browse to the desired location folder in a shell/command prompt
5. Run the following command for the appropriate operating system
Linux
    python3 -m pip install 'PySatLogs @ git+https://gitlab.com/circlemage8/paging-mission-control-tristan-geyster.git' -t .
Windows
    py -m pip install "PySatLogs @ git+https://gitlab.com/circlemage8/paging-mission-control-tristan-geyster.git" -t .
6. Navigate into the Code directory and run the following command
Linux
    python3 main.py
Windows
    py main.py
7. When requested type or paste in the full path and filename of the log file to be processed. Test files are available in the
Test_Input_Files subdirectory on the Gitlab page https://gitlab.com/circlemage8/paging-mission-control-tristan-geyster

Example testing files have been included.
TestInput.txt is the given example
TestInput2.txt is an example where a single satellite and component has multiple instances of flagged problems in 5
minute periods
TestInput3.txt is an example where no entries hit the red flag at all.
TestInput4.txt is an example where there are red flagged entries but not enough in a 5 minute period
